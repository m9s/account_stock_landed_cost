#
msgid ""
msgstr "Content-Type: text/plain; charset=utf-8\n"

msgctxt "field:account.configuration,landed_cost_sequence:"
msgid "Landed Cost Sequence"
msgstr "Secuencia de coste de recepción"

msgctxt "field:account.configuration.landed_cost_sequence,company:"
msgid "Company"
msgstr "Empresa"

msgctxt "field:account.configuration.landed_cost_sequence,create_date:"
msgid "Create Date"
msgstr "Fecha de creación"

msgctxt "field:account.configuration.landed_cost_sequence,create_uid:"
msgid "Create User"
msgstr "Usuario de creación"

msgctxt "field:account.configuration.landed_cost_sequence,id:"
msgid "ID"
msgstr "ID"

msgctxt ""
"field:account.configuration.landed_cost_sequence,landed_cost_sequence:"
msgid "Landed Cost Sequence"
msgstr "Secuencia de coste de recepción"

msgctxt "field:account.configuration.landed_cost_sequence,rec_name:"
msgid "Record Name"
msgstr "Nombre del registro"

msgctxt "field:account.configuration.landed_cost_sequence,write_date:"
msgid "Write Date"
msgstr "Fecha de modificación"

msgctxt "field:account.configuration.landed_cost_sequence,write_uid:"
msgid "Write User"
msgstr "Usuario de modificación"

msgctxt "field:account.invoice.line,landed_cost:"
msgid "Landed Cost"
msgstr "Coste de recepción"

msgctxt "field:account.landed_cost,allocation_method:"
msgid "Allocation Method"
msgstr "Método de asignación"

msgctxt "field:account.landed_cost,company:"
msgid "Company"
msgstr "Empresa"

msgctxt "field:account.landed_cost,create_date:"
msgid "Create Date"
msgstr "Fecha de creación"

msgctxt "field:account.landed_cost,create_uid:"
msgid "Create User"
msgstr "Usuario de creación"

msgctxt "field:account.landed_cost,id:"
msgid "ID"
msgstr "ID"

msgctxt "field:account.landed_cost,invoice_lines:"
msgid "Invoice Lines"
msgstr "Líneas de factura"

msgctxt "field:account.landed_cost,number:"
msgid "Number"
msgstr "Número"

msgctxt "field:account.landed_cost,posted_date:"
msgid "Posted Date"
msgstr "Fecha contabilización"

msgctxt "field:account.landed_cost,rec_name:"
msgid "Record Name"
msgstr "Nombre del registro"

msgctxt "field:account.landed_cost,shipments:"
msgid "Shipments"
msgstr "Albaranes"

msgctxt "field:account.landed_cost,state:"
msgid "State"
msgstr "Estado"

msgctxt "field:account.landed_cost,write_date:"
msgid "Write Date"
msgstr "Fecha de modificación"

msgctxt "field:account.landed_cost,write_uid:"
msgid "Write User"
msgstr "Usuario de modificación"

msgctxt "field:account.landed_cost-stock.shipment.in,create_date:"
msgid "Create Date"
msgstr "Fecha de creación"

msgctxt "field:account.landed_cost-stock.shipment.in,create_uid:"
msgid "Create User"
msgstr "Usuario de creación"

msgctxt "field:account.landed_cost-stock.shipment.in,id:"
msgid "ID"
msgstr "ID"

msgctxt "field:account.landed_cost-stock.shipment.in,landed_cost:"
msgid "Landed Cost"
msgstr "Coste de recepción"

msgctxt "field:account.landed_cost-stock.shipment.in,rec_name:"
msgid "Record Name"
msgstr "Nombre del registro"

msgctxt "field:account.landed_cost-stock.shipment.in,shipment:"
msgid "Shipment"
msgstr "Albarán"

msgctxt "field:account.landed_cost-stock.shipment.in,write_date:"
msgid "Write Date"
msgstr "Fecha de modificación"

msgctxt "field:account.landed_cost-stock.shipment.in,write_uid:"
msgid "Write User"
msgstr "Usuario de modificación"

msgctxt "field:product.product,landed_cost:"
msgid "Landed Cost"
msgstr "Coste de recepción"

msgctxt "field:product.template,landed_cost:"
msgid "Landed Cost"
msgstr "Coste de recepción"

msgctxt "field:stock.move,unit_landed_cost:"
msgid "Unit Landed Cost"
msgstr "Unidad coste de recepción"

msgctxt "model:account.configuration.landed_cost_sequence,name:"
msgid "Account Configuration Landed Cost Sequence"
msgstr "Configuración de la secuencia de coste de recepción"

msgctxt "model:account.landed_cost,name:"
msgid "Landed Cost"
msgstr "Coste de recepción"

msgctxt "model:account.landed_cost-stock.shipment.in,name:"
msgid "Landed Cost - Shipment"
msgstr "Coste de recepción - Albarán"

msgctxt "model:ir.action,name:act_landed_cost_form"
msgid "Landed Costs"
msgstr "Costes de recepción"

msgctxt ""
"model:ir.action.act_window.domain,name:act_landed_cost_form_domain_all"
msgid "All"
msgstr "Todo"

msgctxt ""
"model:ir.action.act_window.domain,name:act_landed_cost_form_domain_draft"
msgid "Draft"
msgstr "Borrador"

msgctxt ""
"model:ir.action.act_window.domain,name:act_landed_cost_form_domain_posted"
msgid "Posted"
msgstr "Contabilizado"

msgctxt "model:ir.model.button,confirm:landed_cost_post_button"
msgid "Are you sure you want to post the landed cost?"
msgstr "Seguro que quieres contabilizar el coste de recepción?"

msgctxt "model:ir.model.button,string:landed_cost_cancel_button"
msgid "Cancel"
msgstr "Cancelar"

msgctxt "model:ir.model.button,string:landed_cost_draft_button"
msgid "Draft"
msgstr "Borrador"

msgctxt "model:ir.model.button,string:landed_cost_post_button"
msgid "Post"
msgstr "Contabilizar"

msgctxt "model:ir.rule.group,name:rule_group_landed_cost"
msgid "User in company"
msgstr "Usuario en la empresa"

msgctxt "model:ir.sequence,name:sequence_landed_cost"
msgid "Landed Cost"
msgstr "Coste de recepción"

msgctxt "model:ir.sequence.type,name:sequence_type_landed_cost"
msgid "Landed Cost"
msgstr "Coste de recepción"

msgctxt "model:ir.ui.menu,name:menu_landed_cost"
msgid "Landed Costs"
msgstr "Costes de recepción"

msgctxt "selection:account.landed_cost,allocation_method:"
msgid "By Value"
msgstr "Por valor"

msgctxt "selection:account.landed_cost,state:"
msgid "Canceled"
msgstr "Cancelados"

msgctxt "selection:account.landed_cost,state:"
msgid "Draft"
msgstr "Borrador"

msgctxt "selection:account.landed_cost,state:"
msgid "Posted"
msgstr "Contabilizados"

msgctxt "view:account.configuration:"
msgid "Landed Cost"
msgstr "Coste de recepción"

msgctxt "view:account.configuration:"
msgid "Sequence"
msgstr "Secuencia"

msgctxt "view:account.landed_cost:"
msgid "Cancel"
msgstr "Cancelar"

msgctxt "view:account.landed_cost:"
msgid "Draft"
msgstr "Borrador"

msgctxt "view:account.landed_cost:"
msgid "Post"
msgstr "Contabilizar"
